%% Question 3
% Le découplage permet de compenser l'effet non-linéraire autour d'une
% configuration d'équilibre. Pour cela, on chercher à s'affranchir des
% termes non-linéaires inertiels.
%
% Si 
% $$ \cos(\theta_2') = 1 $$
%
% $$ \sin(\theta_2') = 0 $$
%
% $$ \cos(\theta_1') = 1 $$
%
% %% \cos(\theta_1' + \theta_2) = 1 %$
%
% Il faut dont que :
% $$ \theta_2') = k\pi , \forall k $$
%
% $$ \theta_2' = \theta_2 - \theta_1 $$ 
% 
% $$ \theta_1'= \theta_1' = k\pi, \forall k $$
% 
% $$ \theta_2 = k\pi $$
%
%