%% Question 8
% 
% D'après l'équation (5) :
%
% $$ \pmatrix{N_1K_{c1}i_1 \cr N_2K_{c2}i_2}  - g(q) \Gamma_f = \pmatrix{I_1'+m_2I_1^2+I_{a1} & h \cos(\theta_2-\theta_1) \cr h \cos(\theta_2-\theta_1) & I_2'+I_{a2}}
% \pmatrix{\ddot{\theta_1} \cr \ddot{\theta_2}}
% + \pmatrix{-h\sin(\theta_2-\theta_1)\dot{\theta_2}^2 
% \cr
% h\sin(\theta_2-\theta_1)\dot{\theta_1}^2} $$
%
% On peut l'écrire sous la forme suivante : 
%
% $$ \pmatrix{N_1K_{c1}i_1 \cr N_2K_{c2}i_2}  - g(q) \Gamma_f = $$
% $$  \pmatrix{(I_1'+m_2I_1^2+I_{a1})\ddot{\theta_1} +  h \cos(\theta_2-\theta_1) \ddot{\theta_2}-h\sin(\theta_2-\theta_1)\dot{\theta_2}^2 
% \cr
% h \cos(\theta_2-\theta_1)\ddot{\theta_1} + (I_2'+I_{a2})\ddot{\theta_2} +
% h\sin(\theta_2-\theta_1)\dot{\theta_1}^2} $$
%
% $$=  \pmatrix{(I_1'+m_2I_1^2+I_{a1})\ddot{\theta_1} +  h (\cos(\theta_2-\theta_1) \ddot{\theta_2}-\sin(\theta_2-\theta_1)\dot{\theta_2}^2)  + 0\times(I_2'+I_{a2})
% \cr
% 0\times(I_1'+m_2I_1^2+I_{a1}) + h (\cos(\theta_2-\theta_1)\ddot{\theta_1} + \sin(\theta_2-\theta_1)\dot{\theta_1}^2) + (I_2'+I_{a2})\ddot{\theta_2}} $$
%
% $$=  \pmatrix{\ddot{\theta_1} &  \cos(\theta_2-\theta_1) \ddot{\theta_2}-\sin(\theta_2-\theta_1)\dot{\theta_2}^2  & 0
% \cr
% 0 & \cos(\theta_2-\theta_1)\ddot{\theta_1} + \sin(\theta_2-\theta_1)\dot{\theta_1}^2 & \ddot{\theta_2}}
% \pmatrix{I_1'+m_2I_1^2+I_{a1} \cr h \cr I_2'+I_{a2}} $$
%
% CQFD.
%%