%% Question 5
% 
% Comme la position de 
% $$ \theta_1 $$ 
% est constante, alors : 
% $$ \dot \theta_{1} =  \ddot \theta_{1}= 0 $$
%
% et comme la vitesse de l'axe de rotation 2 est constante, alors :
% $$ \dot \theta_{2} = constante $$ 
% $\Rightarrow$
% $$ \ddot \theta_{2} = 0 $$ 
%
% Ainsi, nous retrouvons la formule suivante :
% $$ \ddot \theta_{1} =  \dot \theta_{2}= 0$
%  et
%
% $$ N_2K_{c_2}i_2 = \pmatrix{cos(\theta_2) & sign(\dot{\theta}_2) & \dot{\theta}_2 & 1 \cr}
% \pmatrix{\alpha_2 \cr a_2 \cr b_2 \cr c_2} $$
%
%
%
% En posant $\theta_2 = \pi/2$, il est possible de déterminer $c_2$ .
% Ensuite, on peut facilement déterminer $\alpha_2$ en posant $\theta_2 =
% 0$ et en soustrayant $c_2$ à la valeur obtenue.
%
% On peut ensuite déterminer $b_2$ et $c_2$ en faisant varier la vitesse $\dot \theta_2$ et en trouvant ces paramètres par résolution de systèmes d'au moins 2 équations (pour résoudre les 2 inconnues).
%
%% Question 7
% 
% 
% Pour faire de même avec  
% $$ N_1K_{c_1}i_1 $$
% , il est nécessaire de se placer dans le cas où le bras 1 est immobile
% et le bras 2 se déplace avec une vitesse angulaire constante. Donc :
%
% $$ \dot \theta_{1} =  \ddot \theta_{1}= 0 $$
% et
% $$ \ddot \theta_{2} = 0 $$
%
% 
% Ainsi on a,
%
% $$ N_1K_{c_1}i_1 = \pmatrix{cos(\theta_1) & sign(\dot{\theta}_1) & \dot{\theta}_1 & 1 \cr}
% \pmatrix{g(m_2l_1 +m_1\lambda_1) \cr a_1 \cr b_1 \cr c_1} $$
% 
%
% En posant 
% $$\theta_1 = \pi/2$
% il est possible de déterminer 
% $$c_1$
% .
% Ensuite, on peut facilement déterminer 
% $$\lambda_1$
% en posant 
% $$\theta_1 = 0$
% . On peut ensuite déterminer 
% $$b_1, c_1$
% en faisant varier la vitesse
% $$\dot \theta_1$
% et en trouvant ces paramètres par résolution de systèmes d'au moins 2 équations (pour résoudre les 2 inconnues).
%%